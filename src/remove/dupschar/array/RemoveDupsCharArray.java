/*
Write a program/method that takes a char array and removes duplicate letters.
Example char[] str = {'a','b','a','c', 'k', 'a', 'j', 'b'};

the result would be 
Before = abackajb
After = abckj

*/

package remove.dupschar.array;

class RemoveDupsCharArray {

    public static void main(String[] args) {
        char[] str = {'a', 'b', 'a', 'c', 'k', 'a', 'j', 'b'};
        System.out.println("Before = " + (new String(str)));

        for (int first = 0; first < str.length; first++) {
            for (int second = first + 1; second < str.length; second++) {
                if (str[first] == str[second]) {
                    str[second] = 0;
                }
            }
        }

        System.out.println("After = " + (new String(str)));

    }
}
