/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dupschar.array;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author appleman
 */
public class RemoveDupStrings {

    public static void main(String[] args) {
        String[] str = {"a", "b", "a", "c", "k", "a", "j", "b"};
        String joinedString = StringUtils.join(str, "");
        System.out.println("joinedString = " + joinedString + "------");
        System.out.println("str.length = " + str.length);

        for (int first = 0; first < str.length; first++) {
            for (int second = first + 1; second < str.length; second++) {
                if (str[first].equals(str[second])) {
                    str[second] = "";
                }
            }
        }
        joinedString = StringUtils.join(str, "");
        System.out.println("joinedString = " + joinedString + "------");
                System.out.println("str.length = " + str.length);


    }
}